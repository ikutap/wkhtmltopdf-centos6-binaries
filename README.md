wkhtmltopdf binaries
================

This is a fork from [h4cc/wkhtmltopdf-amd64](https://github.com/h4cc/wkhtmltopdf-amd64).

This Repo contains the CentOS 6 binaries from the [wkhtmltopdf project](http://wkhtmltopdf.org/).
More about the functionality of wkhtmltopdf and wkthmltoimage can be found there.

## Installation

_Hint_:
The version of the binary is equal to the git tag.
To install the latest version, use '0.12.1'.

In case this package does _not_ work on your system, try installing the matching system packages from here: [http://wkhtmltopdf.org/downloads.html](http://wkhtmltopdf.org/downloads.html).

### Packagist

This package can be found on [Packagist](http://packagist.org) and installed with [Composer](http://getcomposer.com/).

Require the package for _amd64_ with:

    php composer.phar require ikutap/wkhtmltopdf-centos6-binaries "0.12.1"

The binary will then be located at:

    vendor/ikutap/wkhtmltopdf-centos6-binaries/bin/wkhtmltopdf

Also a symlink will be created in your configured bin/ folder, for example:

    vendor/bin/wkhtmltopdf
